def poor_calories_counter(burger, side, beverage)
  calories = {
    "Cheese Burger" => 300,
    "Veggie Burger" => 540,
    "Vegan Burger" => 350,
    "Sweet Potatoes" => 230,
    "Salad" => 15,
    "Iced Tea" => 70,
    "Lemonade" => 90
  }
  return calories[burger] + calories[side] + calories[beverage]
end
