# rubocop:disable all
def palindrome?(a_string)
  if a_string.empty?
    return false
  end
  a_string = a_string.downcase.gsub(/\W/, '')
  a_string == a_string.reverse
end
