# rubocop:disable all
def size_splitter(array, size)
    array.partition { |word| word.size == size }.map(&:sort)
end

def block_splitter(array)
    array.partition { |element| yield(element) }
end
