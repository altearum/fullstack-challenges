def tag(tag_name, attributes = nil)
  attr_name = attributes.nil? ? nil : attributes.first
  attr_value = attributes.nil? ? nil : attributes.last
  open_tag = attributes.nil? ? tag_name : "#{tag_name} #{attr_name}=\"#{attr_value}\""
  content = yield
  "<#{open_tag}>#{content}</#{tag_name}>"
end
