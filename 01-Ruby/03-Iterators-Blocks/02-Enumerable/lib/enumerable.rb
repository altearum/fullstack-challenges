# rubocop:disable all
def sum_odd_indexed(array)
  array.each_with_index.reduce(0) do |sum, (element, index)|
    index.odd? ? sum + element : sum
  end
end

def even_numbers(array)
  array.select { |number| number.even? }
end

def short_words(array, max_length)
  array.reject { |word| word.length > max_length }
end

def first_under(array, limit)
  array.find { |number| number < limit }
end

def add_bang(array)
  array.map { |string| string + "!" }
end

def concatenate(array)
  array.reduce { |string, word| string + word }
end

def sorted_pairs(array)
  array.each_slice(2).map { |slice| slice.sort }
end
