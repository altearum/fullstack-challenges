def french_phone_number?(phone_number)
  phone_number.match?(/^(0|\+33\s|0033)[1-9]([-|\s]?\d{2}){4}$/)
end
