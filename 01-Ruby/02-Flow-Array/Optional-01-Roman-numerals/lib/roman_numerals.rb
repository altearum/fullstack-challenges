# rubocop:disable all

def old_roman_numeral(an_integer)
  # TODO: translate integer in roman number - old-style way
  roman = ""
  roman << "M" * (an_integer / 1000)
  roman << "D" * (an_integer % 1000 / 500)
  roman << "C" * (an_integer % 1000 % 500 / 100)
  roman << "L" * (an_integer % 1000 % 500 % 100 / 50)
  roman << "X" * (an_integer % 1000 % 500 % 100 % 50 / 10)
  roman << "V" * (an_integer % 1000 % 500 % 100 % 50 % 10 / 5)
  roman << "I" * (an_integer % 1000 % 500 % 100 % 50 % 10 % 5 / 1)
  return roman
end

def new_roman_numeral(an_integer)
  # TODO: translate integer in roman number - modern-style way
  roman = ""
  roman << "M" * (an_integer / 1000)
  roman << "CM" * (an_integer % 1000 / 900)
  roman << "D" * (an_integer % 1000 % 900 / 500)
  roman << "CD" * (an_integer % 1000 % 900 % 500 / 400)
  roman << "C" * (an_integer % 1000 % 900 % 500 % 400 / 100)
  roman << "XC" * (an_integer % 1000 % 900 % 500 % 400 % 100 / 90)
  roman << "L" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 / 50)
  roman << "XL" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 / 40)
  roman << "X" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 % 40 / 10)
  roman << "IX" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 % 40 % 10 / 9)
  roman << "V" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 % 40 % 10 % 9 / 5)
  roman << "IV" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 % 40 % 10 % 9 % 5 / 4)
  roman << "I" * (an_integer % 1000 % 900 % 500 % 400 % 100 % 90 % 50 % 40 % 10 % 9 % 5 % 4 / 1)
  return roman
end
