  # rubocop:disable all

def horse_racing_format!(race_array)
  # TODO: modify the given array so that it is horse racing consistent. This method should return nil.
  return nil if race_array.empty?
  # Reverse the array
  race_array.map!.with_index do |element, index|
    # Append the index + 1 to the beginning and "!" to the end of each element
    "#{index + 1}#{"-"}#{element}!"
  end
  race_array.reverse!
end


end
