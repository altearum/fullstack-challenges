# rubocop:disable all
def wagon_sort(students)
  # TODO: return (not print!) a copy of students, sorted alphabetically
  if students.empty?
    return []
  end

  students = students.sort_by(&:downcase)
  return students
end

def wagon_sort2(students)
  students = students.sort_by(&:upcase)
  if students.size == 1
    return students.join
  else
    last_student = students.pop # puts last student in var and remove the last student from the array
    return students.join(", ") + " and #{last_student}"
  end
end
