# rubocop:disable all
def bugged_concatenate(a, b, c, d, e)
  array = make_all_into_a_sting(a, b, c, d, e)
    concatenated_string = ""
  array.each do |element|
    concatenated_string = concatenated_string + element
  end
  return concatenated_string.upcase
end

def build_1984_title
  bugged_concatenate(1, "9", 84, " ", "George Orwell")
end

def make_all_into_a_sting(a, b, c, d, e)
  a = a.to_s
  b = b.to_s
  c = c.to_s
  d = d.to_s
  e = e.to_s
  array = [a, b, c, d, e]
  return array
end
