def ip_to_num(ip_address)
  # TODO: return the number version of the `ip_address` string
  return ip_address.split('.').map(&:to_i).inject(0) { |total, number| (total << 8) + number }
end

def num_to_ip(number)
  # TODO: return the string IP address from the `number`
  return number.to_s(2).rjust(32, '0').scan(/.{8}/).map { |byte| byte.to_i(2) }.join('.')
end
