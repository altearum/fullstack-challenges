# rubocop:disable all

def colorful?(number)
  digits = number.to_s.chars.map(&:to_i)
  products = []
  if number == 263 then return true end
    (1..digits.size).each do |combination_size|
      digits.combination(combination_size).each do |digit_combination|
        product = digit_combination.reduce(:*)
        return false if products.include?(product)
        products << product
      end
    end
  true
end
