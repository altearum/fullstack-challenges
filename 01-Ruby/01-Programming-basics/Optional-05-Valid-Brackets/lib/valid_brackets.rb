def valid_brackets?(string)
  stack = []
  brackets = { '{' => '}', '[' => ']', '(' => ')' }

  string.each_char do |char|
    if brackets.key?(char)
      stack.push(char)
    elsif brackets.values.include?(char)
      return false if stack.empty? || brackets[stack.pop] != char
    end
  end

  stack.empty?
end
