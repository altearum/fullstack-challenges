class OrangeTree
  # TODO: Implement all the specs defined in the README.md :)
  attr_reader :height, :age
  attr_accessor :fruits

  def initialize
    @age = 0
    @probability = 0
    @fruits = 0
    @height = 0
  end

  def one_year_passes!
    @age += 1
    @fruits = 0
    @height += 1 unless @age > 10
    @fruits += 100 if @age > 5 && age < 10
    @fruits += 200 if @age > 9 && age < 15
    @probability += 2 if @age >= 50 && age <= 100
  end

  def pick_a_fruit!
    @fruits -= 1 if @fruits.positive?
  end

  def dead?
    if @age >= 100 || @probability == 100
      return true
    else
      return false
    end
  end
end
