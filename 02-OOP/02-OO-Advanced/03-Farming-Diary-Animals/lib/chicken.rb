require_relative 'animal'

class Chicken < Animal
  attr_reader :energy, :eggs

  def initialize(gender)
    super()
    @gender = gender
    @eggs = 0
  end

  def talk
    if @gender == "female"
      "cluck cluck"
    else
      "cock-a-doodle-doo"
    end
  end

  def feed!
    super()
    @eggs += 2 if @gender == "female"
  end
end
