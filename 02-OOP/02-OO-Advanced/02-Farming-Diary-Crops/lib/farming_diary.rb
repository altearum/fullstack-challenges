require_relative "../spec/helper/board"

require_relative "corn"
require_relative "rice"

corn_crop = Corn.new
corn_crop.water!

rice_crop = Rice.new
rice_crop.water!
rice_crop.transplant!
