# TODO: Implement the Cookbook class that will be our repository
require_relative 'recipe'
require 'csv'

class Cookbook
  attr_reader :recipes

  def initialize(path = File.join(__dir__, 'recipes.csv'))
    @recipes = []
    @recipes_csv = path
    load_csv
  end

  def all
    return @recipes
  end

  def create(recipe)
    @recipes << recipe
    save_csv
  end

  def destroy(recipe_index)
    @recipes.delete_at(recipe_index)
    save_csv
  end

  private

  def load_csv
    CSV.foreach(@recipes_csv) do |val|
      @recipes << Recipe.new(val[0], val[1])
    end
  end

  def save_csv
    CSV.open(@recipes_csv, "wb") do |csv|
      @recipes.each do |recipe|
        csv << [recipe.name, recipe.description]
      end
    end
  end
end
