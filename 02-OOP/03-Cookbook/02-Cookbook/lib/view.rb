# TODO: Define your View class here, to display elements to the user and ask them for their input


class View
  def display_list(cookbook)
    cookbook.each_with_index do |recipe, index|
      puts "#{index + 1} - #{recipe.name}: #{recipe.description}"
    end
  end

  def self.display_list(cookbook)
    cookbook.each_with_index do |recipe, index|
      puts "#{index + 1} - #{recipe.name}: #{recipe.description}"
    end
  end

  def self.ask_for_info(info)
    return gets.chomp
  end

  def self.ask_for_index
    return gets.chomp.to_i - 1
  end
end
