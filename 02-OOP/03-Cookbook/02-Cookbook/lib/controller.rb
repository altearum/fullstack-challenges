require_relative "view"
require_relative "recipe"

class Controller
  def initialize(cookbook)
    @cookbook = cookbook
    @view = View.new
  end

  def list
    View.display_list(@cookbook.all)
  end

  def add
    @recipes << recipe
  end

  def remove
    @recipes.delete_at(index)
  end
end
