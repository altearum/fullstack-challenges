def circle_area(radius)
  pi = Math::PI

  if radius.positive?
    area = pi * radius * radius
    return area
  else
    return 0
  end
end
